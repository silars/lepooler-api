﻿using LePooler.Common.Extensions;
using System;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace LePooler.Common.JsonConverters
{
    public class JsonDescriptionEnumConverter : JsonConverterFactory
    {
        public override JsonConverter CreateConverter(Type typeToConvert, JsonSerializerOptions options)
        {
            JsonConverter converter = (JsonConverter)Activator.CreateInstance(typeof(JsonDescriptionEnumConverterInner<>).
                MakeGenericType(typeToConvert), BindingFlags.Instance | BindingFlags.Public, binder: null, null, culture: null)!;

            return converter;
        }

        public override bool CanConvert(Type typeToConvert)
        {
            return typeToConvert.IsEnum;
        }

        private class JsonDescriptionEnumConverterInner<TEnum> : JsonConverter<TEnum> where TEnum : struct, Enum
        {
            public override TEnum Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
            {
                return reader.GetString().GetEnumValueFromDescription<TEnum>();
            }

            public override void Write(Utf8JsonWriter writer, TEnum value, JsonSerializerOptions options)
            {
                try
                {
                    writer.WriteStringValue(value.GetDescription());
                }
                catch (Exception)
                {
                    JsonSerializer.Serialize(writer, value, options);
                }
            }
        }
    }
}