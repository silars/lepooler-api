﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;

namespace LePooler.Common.Extensions
{
    public static class GenericExtensions
    {
        public static T Clone<T>(this T source)
        {
            // Don't serialize a null object, simply return the default for that object
            if (source == null)
            {
                return default(T);
            }

            var serializeSettings = new JsonSerializerSettings
            {
                ContractResolver = new IgnoreJsonAttributesResolver(),
                MaxDepth = 1,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            // initialize inner objects individually
            // for example in default constructor some list property initialized with some values,
            // but in 'source' these items are cleaned -
            // without ObjectCreationHandling.Replace default constructor values will be added to result
            var deserializeSettings = new JsonSerializerSettings
            {
                ObjectCreationHandling = ObjectCreationHandling.Replace,
                ContractResolver = new IgnoreJsonAttributesResolver()
            };

            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(source, serializeSettings), deserializeSettings);
        }
    }

    #region Custom ContractResolver to clone objects via Json correctly (i.e.: ignore JsonIgnore attributes)

    internal class IgnoreJsonAttributesResolver : DefaultContractResolver
    {
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            var props = base.CreateProperties(type, memberSerialization);
            foreach (var prop in props)
            {
                prop.Ignored = false;   // Ignore [JsonIgnore]
                prop.Converter = null;  // Ignore [JsonConverter]
                prop.PropertyName = prop.UnderlyingName;  // restore original property name
            }
            return props;
        }
    }

    #endregion Custom ContractResolver to clone objects via Json correctly (i.e.: ignore JsonIgnore attributes)
}