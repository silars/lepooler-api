﻿using System;
using System.ComponentModel;
using System.Linq;

namespace LePooler.Common.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDescription<T>(this T value)
        {
            if (!typeof(T).IsEnum)
                throw new ArgumentException("T must be an enumerated type");
            var type = value.GetType();
            var name = Enum.GetName(type, value);
            if (name == null)
                return null;

            var field = type.GetField(name);
            if (field == null)
                return null;

            if (Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) is DescriptionAttribute attr)
            {
                return attr.Description;
            }
            return null;
        }

        public static T GetEnumValueFromDescription<T>(this string enumDescription)
            where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
                throw new ArgumentException("T must be an enumerated type");
            var enumType = typeof(T);
            return Enum.GetValues(enumType).Cast<T>().ToList().FirstOrDefault(v => v.GetDescription() == enumDescription);
        }
    }
}