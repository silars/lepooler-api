﻿namespace LePooler.Common.Constants
{
    public static class AuthenticationToken
    {
        public const int DurationInMinutes = 1440;
    }
}