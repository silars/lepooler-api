﻿using LePooler.Common.Configuration;
using LePooler.Models.Players;
using LePooler.Models.Poolers;
using LePooler.Models.Pools;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;

namespace LePooler.Data.Contexts
{
    public class LePoolerContext : DbContext
    {
        public DbSet<Player> Players { get; set; }
        public DbSet<Pooler> Poolers { get; set; }
        public DbSet<Pool> Pools { get; set; }

        public LePoolerContext(DbContextOptions<LePoolerContext> options) : base(options)
        { }

        public void UpdatePlayersFromScrapedData()
        {
            var predictionsFilePath = Environment.ExpandEnvironmentVariables(AppConfiguration.PredictionsFileLocation);
            try
            {
                var playersInPredictionsFile = JsonSerializer.Deserialize<List<Player>>(File.ReadAllText(predictionsFilePath), new JsonSerializerOptions(JsonSerializerDefaults.Web));
                foreach (var playerInPredictionsFile in playersInPredictionsFile)
                {
                    var player = Players.Find(playerInPredictionsFile.PlayerNhlId);
                    if (player == null)
                    {
                        Players.Add(playerInPredictionsFile);
                    }
                    else
                    {
                        Entry(player).Collection(p => p.Predictions).Load();
                        Entry(player).CurrentValues.SetValues(playerInPredictionsFile);
                        var predictions = player.Predictions.FirstOrDefault(p => p.Season == playerInPredictionsFile.Predictions.First().Season);
                        if (predictions == null)
                        {
                            player.Predictions.Add(playerInPredictionsFile.Predictions.First());
                            Update(player);
                        }
                        else
                        {
                            Entry(predictions).CurrentValues.SetValues(playerInPredictionsFile.Predictions.First());
                        }
                    }
                }
                SaveChanges();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Pick>()
                .HasKey(p => new { p.PoolGuid, p.PlayerNhlId });

            modelBuilder.Entity<PoolParticipant>()
                .HasKey(p => new { p.PoolGuid, p.PoolerGuid });

            modelBuilder.Entity<PoolParticipant>()
                .HasMany(p => p.Picks)
                .WithOne(p => p.PoolParticipant)
                .HasForeignKey(p => new { p.PoolGuid, p.PoolerGuid });

            modelBuilder.Entity<PlayerPredictions>()
                .HasKey(p => new { p.PlayerNhlId, p.Season });

            modelBuilder.Entity<PlayerPredictions>()
                .HasOne(p => p.Player)
                .WithMany(p => p.Predictions)
                .HasForeignKey(p => p.PlayerNhlId);

            modelBuilder.Entity<Injury>()
                .HasKey(i => new { i.PlayerNhlId, i.StartDate });

            modelBuilder.Entity<Injury>()
                .HasOne(i => i.Player)
                .WithMany(p => p.Injuries)
                .HasForeignKey(i => i.PlayerNhlId);
        }
    }
}