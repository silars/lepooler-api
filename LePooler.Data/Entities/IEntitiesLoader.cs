﻿using LePooler.Models.Poolers;
using LePooler.Models.Pools;
using System;

namespace LePooler.Data.Entities
{
    public interface IEntitiesLoader
    {
        void LoadPoolParticipants(Pool pool, bool loadPoolers = false, bool loadPicks = false, bool loadPlayers = false, bool loadPlayersPredictions = false, bool loadInjuries = false);

        void LoadPoolParticipants(Guid poolGuid, bool loadPoolers = false, bool loadPicks = false, bool loadPlayers = false, bool loadPlayersPredictions = false);

        void LoadPicks(Pool pool, bool loadPoolers = false, bool loadPlayers = false, bool loadPlayersPredictions = false, bool loadInjuries = false);

        void LoadPlayer(Pick pick, bool loadPlayersPredictions = false, bool loadInjuries = false);

        void LoadPooler(Pick pick);

        void LoadPooler(PoolParticipant poolParticipant);

        Pooler LoadAndGetPooler(PoolParticipant poolParticipant);
    }
}