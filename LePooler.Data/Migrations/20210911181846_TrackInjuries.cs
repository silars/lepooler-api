﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LePooler.Data.Migrations
{
    public partial class TrackInjuries : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pick_Pools_PoolGuid",
                table: "Pick");

            migrationBuilder.CreateTable(
                name: "Injury",
                columns: table => new
                {
                    PlayerNhlId = table.Column<string>(type: "varchar(255) CHARACTER SET utf8mb4", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                    EndDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    Description = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Injury", x => new { x.PlayerNhlId, x.StartDate });
                    table.ForeignKey(
                        name: "FK_Injury_Players_PlayerNhlId",
                        column: x => x.PlayerNhlId,
                        principalTable: "Players",
                        principalColumn: "PlayerNhlId",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Injury");

            migrationBuilder.AddForeignKey(
                name: "FK_Pick_Pools_PoolGuid",
                table: "Pick",
                column: "PoolGuid",
                principalTable: "Pools",
                principalColumn: "PoolGuid",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
