﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LePooler.Data.Migrations
{
    public partial class AddSeasonColumnToPool : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Season",
                table: "Pools",
                type: "longtext CHARACTER SET utf8mb4",
                defaultValue: "2020-2021",
                nullable: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Season",
                table: "Pools");
        }
    }
}