﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LePooler.Data.Migrations
{
    public partial class AddSeasonKeyToPredictions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Assists",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "AssistsLastYear",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "Defeats",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "DefeatsLastYear",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "GamesPlayed",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "GamesPlayedLastYear",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "Goals",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "GoalsLastYear",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "OvertimeDefeats",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "OvertimeDefeatsLastYear",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "Points",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "PointsLastYear",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "PointsPerMatch",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "Rank",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "Shutouts",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "ShutoutsLastYear",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "Victories",
                table: "Players");

            migrationBuilder.DropColumn(
                name: "VictoriesLastYear",
                table: "Players");

            migrationBuilder.RenameColumn(
                name: "LogoUrl",
                table: "Players",
                newName: "TeamLogoUrl");

            migrationBuilder.CreateTable(
                name: "PlayerPredictions",
                columns: table => new
                {
                    PlayerNhlId = table.Column<string>(type: "varchar(255) CHARACTER SET utf8mb4", nullable: false),
                    Season = table.Column<string>(type: "varchar(255) CHARACTER SET utf8mb4", nullable: false),
                    Rank = table.Column<int>(type: "int", nullable: false),
                    GamesPlayed = table.Column<int>(type: "int", nullable: true),
                    Goals = table.Column<int>(type: "int", nullable: true),
                    Assists = table.Column<int>(type: "int", nullable: true),
                    Victories = table.Column<int>(type: "int", nullable: true),
                    Defeats = table.Column<int>(type: "int", nullable: true),
                    Shutouts = table.Column<int>(type: "int", nullable: true),
                    OvertimeDefeats = table.Column<int>(type: "int", nullable: true),
                    VictoriesLastYear = table.Column<int>(type: "int", nullable: true),
                    DefeatsLastYear = table.Column<int>(type: "int", nullable: true),
                    ShutoutsLastYear = table.Column<int>(type: "int", nullable: true),
                    OvertimeDefeatsLastYear = table.Column<int>(type: "int", nullable: true),
                    Points = table.Column<int>(type: "int", nullable: true),
                    PointsPerMatch = table.Column<double>(type: "double", nullable: true),
                    GamesPlayedLastYear = table.Column<int>(type: "int", nullable: true),
                    GoalsLastYear = table.Column<int>(type: "int", nullable: true),
                    AssistsLastYear = table.Column<int>(type: "int", nullable: true),
                    PointsLastYear = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerPredictions", x => new { x.PlayerNhlId, x.Season });
                    table.ForeignKey(
                        name: "FK_PlayerPredictions_Players_PlayerNhlId",
                        column: x => x.PlayerNhlId,
                        principalTable: "Players",
                        principalColumn: "PlayerNhlId",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PlayerPredictions");

            migrationBuilder.RenameColumn(
                name: "TeamLogoUrl",
                table: "Players",
                newName: "LogoUrl");

            migrationBuilder.AddColumn<int>(
                name: "Assists",
                table: "Players",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AssistsLastYear",
                table: "Players",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Defeats",
                table: "Players",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DefeatsLastYear",
                table: "Players",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "GamesPlayed",
                table: "Players",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "GamesPlayedLastYear",
                table: "Players",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Goals",
                table: "Players",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "GoalsLastYear",
                table: "Players",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OvertimeDefeats",
                table: "Players",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OvertimeDefeatsLastYear",
                table: "Players",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Points",
                table: "Players",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PointsLastYear",
                table: "Players",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "PointsPerMatch",
                table: "Players",
                type: "double",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Rank",
                table: "Players",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Shutouts",
                table: "Players",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ShutoutsLastYear",
                table: "Players",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Victories",
                table: "Players",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "VictoriesLastYear",
                table: "Players",
                type: "int",
                nullable: true);
        }
    }
}