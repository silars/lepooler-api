﻿using LePooler.Models.Pools;
using System;
using System.Collections.Generic;

namespace LePooler.Logic.Pools.PlayerSelection
{
    public interface IPicksLogic
    {
        public ICollection<PoolParticipant> GetPoolParticipantsInPickOrder(Pool pool);

        public Dictionary<int, Guid> GetNextPoolersToPick(int? nbNextPicks, Guid poolGuid);

        public Dictionary<int, Guid> GetNextPoolParticipantsToPick(int? nbNextPicks, Pool pool);
    }
}