﻿using LePooler.Common.Extensions;
using LePooler.Data.Contexts;
using LePooler.Data.Entities;
using LePooler.Models.Players;
using LePooler.Models.Pools;
using System;
using System.Collections.Generic;

namespace LePooler.Logic.Pools.PlayerSelection
{
    public class PicksLogic : IPicksLogic
    {
        private LePoolerContext _context;
        private IEntitiesLoader _entitiesLoader;

        public PicksLogic(LePoolerContext context, IEntitiesLoader entitiesLoader)
        {
            _context = context;
            _entitiesLoader = entitiesLoader;
        }

        public ICollection<PoolParticipant> GetPoolParticipantsInPickOrder(Pool pool)
        {
            _entitiesLoader.LoadPoolParticipants(pool, true, true, true, true, true);
            return pool.GetPoolParticipantsInPickOrder();
        }

        public Dictionary<int, Guid> GetNextPoolersToPick(int? nbNextPicks, Guid poolGuid)
        {
            var pool = _context.Pools.Find(poolGuid);
            return GetNextPoolParticipantsToPick(nbNextPicks, pool);
        }

        public Dictionary<int, Guid> GetNextPoolParticipantsToPick(int? nbNextPicks, Pool pool)
        {
            _entitiesLoader.LoadPoolParticipants(pool, loadPoolers: true);
            if (!nbNextPicks.HasValue)
                nbNextPicks = pool.PoolParticipants.Count;

            _entitiesLoader.LoadPicks(pool);
            var nextPoolersToPick = new Dictionary<int, Guid>();
            var simulatedPool = pool.Clone();
            while (nextPoolersToPick.Count < nbNextPicks)
            {
                var simulatedNextPoolerToPick = simulatedPool.GetNextPoolParticipantToPick();
                if (simulatedNextPoolerToPick == null)
                    break; //No more picks
                nextPoolersToPick.Add(nextPoolersToPick.Count + 1, _entitiesLoader.LoadAndGetPooler(simulatedNextPoolerToPick).PoolerGuid);
                simulatedPool.PickPlayer(new Player
                {
                    PlayerNhlId = Guid.NewGuid().ToString()
                }); //Just adding anything so the pick is counted.
            }

            return nextPoolersToPick;
        }
    }
}