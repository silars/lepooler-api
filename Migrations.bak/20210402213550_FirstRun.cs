﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LePooler.API.Migrations
{
    public partial class FirstRun : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_PoolParticipant_PoolerGuid_PoolGuid",
                table: "PoolParticipant");

            migrationBuilder.CreateIndex(
                name: "IX_PoolParticipant_PoolerGuid",
                table: "PoolParticipant",
                column: "PoolerGuid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PoolParticipant_PoolerGuid",
                table: "PoolParticipant");

            migrationBuilder.AddUniqueConstraint(
                name: "AK_PoolParticipant_PoolerGuid_PoolGuid",
                table: "PoolParticipant",
                columns: new[] { "PoolerGuid", "PoolGuid" });
        }
    }
}
