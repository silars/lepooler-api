﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LePooler.API.Migrations
{
    public partial class Create : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Players",
                columns: table => new
                {
                    PlayerNhlId = table.Column<string>(nullable: false),
                    Rank = table.Column<int>(nullable: false),
                    Position = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Team = table.Column<string>(nullable: true),
                    TeamAbbreviation = table.Column<string>(nullable: true),
                    LogoUrl = table.Column<string>(nullable: true),
                    GamesPlayed = table.Column<int>(nullable: true),
                    Goals = table.Column<int>(nullable: true),
                    Assists = table.Column<int>(nullable: true),
                    Victories = table.Column<int>(nullable: true),
                    Defeats = table.Column<int>(nullable: true),
                    Shutouts = table.Column<int>(nullable: true),
                    OvertimeDefeats = table.Column<int>(nullable: true),
                    VictoriesLastYear = table.Column<int>(nullable: true),
                    DefeatsLastYear = table.Column<int>(nullable: true),
                    ShutoutsLastYear = table.Column<int>(nullable: true),
                    OvertimeDefeatsLastYear = table.Column<int>(nullable: true),
                    Points = table.Column<int>(nullable: true),
                    PointsPerMatch = table.Column<double>(nullable: true),
                    GamesPlayedLastYear = table.Column<int>(nullable: true),
                    GoalsLastYear = table.Column<int>(nullable: true),
                    AssistsLastYear = table.Column<int>(nullable: true),
                    PointsLastYear = table.Column<int>(nullable: true),
                    Age = table.Column<int>(nullable: false),
                    Height = table.Column<string>(nullable: true),
                    Weight = table.Column<int>(nullable: false),
                    Salary = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Players", x => x.PlayerNhlId);
                });

            migrationBuilder.CreateTable(
                name: "Poolers",
                columns: table => new
                {
                    PoolerGuid = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Poolers", x => x.PoolerGuid);
                });

            migrationBuilder.CreateTable(
                name: "Pools",
                columns: table => new
                {
                    PoolGuid = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NbGoalerPicks = table.Column<int>(nullable: false),
                    NbOffenseDefensePicks = table.Column<int>(nullable: false),
                    PickTimeLimitInSeconds = table.Column<int>(nullable: false),
                    Salt = table.Column<string>(nullable: true),
                    HashedPassword = table.Column<byte[]>(nullable: true),
                    LastPickPoolGuid = table.Column<Guid>(nullable: true),
                    LastPickPlayerNhlId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pools", x => x.PoolGuid);
                });

            migrationBuilder.CreateTable(
                name: "Pick",
                columns: table => new
                {
                    PoolGuid = table.Column<Guid>(nullable: false),
                    PoolerGuid = table.Column<Guid>(nullable: false),
                    PlayerNhlId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pick", x => new { x.PoolGuid, x.PlayerNhlId });
                    table.ForeignKey(
                        name: "FK_Pick_Players_PlayerNhlId",
                        column: x => x.PlayerNhlId,
                        principalTable: "Players",
                        principalColumn: "PlayerNhlId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Pick_Pools_PoolGuid",
                        column: x => x.PoolGuid,
                        principalTable: "Pools",
                        principalColumn: "PoolGuid",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Pick_Poolers_PoolerGuid",
                        column: x => x.PoolerGuid,
                        principalTable: "Poolers",
                        principalColumn: "PoolerGuid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PoolParticipant",
                columns: table => new
                {
                    PoolGuid = table.Column<Guid>(nullable: false),
                    PoolerGuid = table.Column<Guid>(nullable: false),
                    PickOrderPosition = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PoolParticipant", x => new { x.PoolGuid, x.PoolerGuid });
                    table.UniqueConstraint("AK_PoolParticipant_PoolerGuid_PoolGuid", x => new { x.PoolerGuid, x.PoolGuid });
                    table.ForeignKey(
                        name: "FK_PoolParticipant_Pools_PoolGuid",
                        column: x => x.PoolGuid,
                        principalTable: "Pools",
                        principalColumn: "PoolGuid",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PoolParticipant_Poolers_PoolerGuid",
                        column: x => x.PoolerGuid,
                        principalTable: "Poolers",
                        principalColumn: "PoolerGuid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Pick_PlayerNhlId",
                table: "Pick",
                column: "PlayerNhlId");

            migrationBuilder.CreateIndex(
                name: "IX_Pick_PoolerGuid",
                table: "Pick",
                column: "PoolerGuid");

            migrationBuilder.CreateIndex(
                name: "IX_Pools_LastPickPoolGuid_LastPickPlayerNhlId",
                table: "Pools",
                columns: new[] { "LastPickPoolGuid", "LastPickPlayerNhlId" });

            migrationBuilder.AddForeignKey(
                name: "FK_Pools_Pick_LastPickPoolGuid_LastPickPlayerNhlId",
                table: "Pools",
                columns: new[] { "LastPickPoolGuid", "LastPickPlayerNhlId" },
                principalTable: "Pick",
                principalColumns: new[] { "PoolGuid", "PlayerNhlId" },
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Pick_Players_PlayerNhlId",
                table: "Pick");

            migrationBuilder.DropForeignKey(
                name: "FK_Pick_Pools_PoolGuid",
                table: "Pick");

            migrationBuilder.DropTable(
                name: "PoolParticipant");

            migrationBuilder.DropTable(
                name: "Players");

            migrationBuilder.DropTable(
                name: "Pools");

            migrationBuilder.DropTable(
                name: "Pick");

            migrationBuilder.DropTable(
                name: "Poolers");
        }
    }
}
