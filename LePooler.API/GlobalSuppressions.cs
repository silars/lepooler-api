﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "<Pending>", Scope = "member", Target = "~M:LePooler.API.Controllers.PoolsController.CreatePool(LePooler.API.Models.Pool)~Microsoft.AspNetCore.Mvc.ActionResult{LePooler.API.Models.Pool}")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification = "<Pending>", Scope = "member", Target = "~P:LePooler.API.Models.Pool.HashedPassword")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1044:Properties should not be write only", Justification = "<Pending>", Scope = "member", Target = "~P:LePooler.API.Models.Pool.PasswordSetter")]
[assembly: SuppressMessage("Reliability", "CA2007:Do not directly await a Task", Justification = "<Pending>")]