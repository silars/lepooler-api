﻿using LePooler.Data.Contexts;
using LePooler.Data.Entities;
using LePooler.Models.Players;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LePooler.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PlayersController : BaseController
    {
        public PlayersController(LePoolerContext context, IEntitiesLoader entitiesLoader) : base(context, entitiesLoader)
        {
        }

        [HttpGet]
        public ActionResult<IEnumerable<Player>> Get()
        {
            return Ok(Context.Players.Include(p => p.Injuries));
        }

        [HttpGet("{playerNhlId}")]
        public ActionResult<Player> Get(string playernhlId)
        {
            try
            {
                return Ok(Context.Players.Include(p => p.Injuries).Single(p => p.PlayerNhlId == playernhlId));
            }
            catch (InvalidOperationException)
            {
                return DoesNotExist("joueur");
            }
        }

        [HttpGet("{team}/{name}")]
        public ActionResult<Player> Get(string team, string name)
        {
            try
            {
                return Ok(Context.Players.Include(p => p.Injuries).Single(p => p.Team == team && p.Name == name));
            }
            catch (InvalidOperationException)
            {
                return DoesNotExist("joueur");
            }
        }

        [HttpGet("predictions/{season}")]
        public ActionResult<IEnumerable<Player>> GetPredictions(string season)
        {
            Common.Utilities.Validation.ValidateSeason(season);
            return Ok(Context.Players.Where(p => p.Predictions.Any(x => x.Season == season)).Include(p => p.Injuries).Include(p => p.Predictions.Where(x => x.Season == season)));
        }

        [HttpGet("{playerNhlId}/predictions/{season}")]
        public ActionResult<IEnumerable<Player>> GetPredictions(string playernhlId, string season)
        {
            Common.Utilities.Validation.ValidateSeason(season);
            return Ok(Context.Players.Where(p => p.PlayerNhlId == playernhlId && p.Predictions.Any(x => x.Season == season)).Include(p => p.Injuries).Include(p => p.Predictions.Where(x => x.Season == season)));
        }

        [HttpGet("refresh")]
        public ActionResult<IEnumerable<Player>> Refresh()
        {
            Context.UpdatePlayersFromScrapedData();
            return Get();
        }
    }
}