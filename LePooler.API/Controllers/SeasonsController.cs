﻿using LePooler.Data.Contexts;
using LePooler.Data.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace LePooler.API.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class SeasonsController : BaseController
    {
        public SeasonsController(LePoolerContext context, IEntitiesLoader entitiesLoader) : base(context, entitiesLoader)
        {
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(Context.Players.Include(p => p.Predictions).SelectMany(p => p.Predictions.Select(x => x.Season).Distinct()).Distinct().OrderByDescending(s => s));
        }
    }
}