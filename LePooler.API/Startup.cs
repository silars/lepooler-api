﻿using LePooler.API.ActionFilters;
using LePooler.API.BackgroundServices;
using LePooler.API.Hubs;
using LePooler.Common.Configuration;
using LePooler.Common.Constants;
using LePooler.Data.Contexts;
using LePooler.Data.Entities;
using LePooler.Logic.Pools.PlayerSelection;
using LePooler.Scrapers.Interfaces;
using LePooler.Scrapers.Marqueur;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System.Linq;
using System.Text;

namespace LePooler.API
{
    public class Startup
    {
        private readonly IWebHostEnvironment _hostingEnvironment;

        public Startup(IConfiguration configuration, IWebHostEnvironment hostingEnvironment)
        {
            Configuration = configuration;
            _hostingEnvironment = hostingEnvironment;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                if (_hostingEnvironment.IsDevelopment())
                {
                    options.AddPolicy("AllowSpecificOriginLocal",
                        builder => builder
                            .WithOrigins("http://localhost:4200")
                            .WithHeaders("authorization", "accept", "content-type", "origin", "x-requested-with", "x-signalr-user-agent")
                            .WithMethods("GET", "POST", "PUT", "DELETE")
                            .AllowCredentials()
                        );
                }
                else
                {
                    options.AddPolicy("AllowSpecificOrigin",
                        builder => builder
                            .WithOrigins(AppConfiguration.CorsAllowedOrigins.ToArray())
                            .WithHeaders("authorization", "accept", "content-type", "origin", "x-requested-with", "x-signalr-user-agent")
                            .WithMethods("GET", "POST", "PUT", "DELETE", "OPTIONS")
                            .AllowCredentials()
                        );
                }
            });

            var connectionString = $"server={AppConfiguration.MySqlHost}; port={AppConfiguration.MySqlPort}; database={AppConfiguration.MySqlDatabase}; user={AppConfiguration.MySqlUser}; password={AppConfiguration.MySqlPassword}; Persist Security Info=false; Connect Timeout=300";
            services.AddDbContext<LePoolerContext>(opt => opt.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString)));

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = !_hostingEnvironment.IsDevelopment(),
                        ValidateAudience = !_hostingEnvironment.IsDevelopment(),
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuers = AppConfiguration.JwtValidIssuers,
                        ValidAudiences = AppConfiguration.JwtValidAudiences,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(AppConfiguration.JwtSigningKey))
                    };
                });
            services.AddAuthorization(options =>
            {
                options.AddPolicy(Policies.Authenticated, policy => policy.RequireAssertion(context => context.User.Claims.Any()).Build());
                options.AddPolicy(Policies.PoolAdmin, policy => policy.RequireClaim(Policies.PoolAdmin).Build());
                options.AddPolicy(Policies.PoolSpectate, policy => policy.RequireClaim(Policies.PoolSpectate).Build());
                options.AddPolicy("DevOnly", policy => policy.RequireAssertion(x => _hostingEnvironment.IsDevelopment()));
            });

            services.AddMvc(options => options.EnableEndpointRouting = false).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddSignalR(o =>
            {
                o.EnableDetailedErrors = _hostingEnvironment.IsDevelopment();
            });

            services.AddControllers(options => options.Filters.Add(new ExceptionFilter()));

            ConfigureBackgroundServices(services);
            ConfigureDependencyInjection(services);
        }

        private void ConfigureBackgroundServices(IServiceCollection services)
        {
            services.AddHostedService<UpdateInjuriesBackgroundService>();
        }

        private void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddScoped<IInjuriesScraper, MarqueurInjuriesScraper>();

            services.AddScoped<IEntitiesLoader, EntitiesLoader>();
            services.AddScoped<IPicksLogic, PicksLogic>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public static void Configure(IApplicationBuilder app, IWebHostEnvironment env, LePoolerContext dbContext)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("AllowSpecificOrigin");
            app.UseCors("AllowSpecificOriginLocal");

            dbContext.Database.Migrate();
            dbContext.UpdatePlayersFromScrapedData();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseMvc();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<SpectateHub>("/spectate");
            });
        }
    }
}