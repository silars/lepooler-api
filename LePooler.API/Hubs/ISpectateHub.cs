﻿using LePooler.Models.Players;
using LePooler.Models.Pools;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LePooler.API.Hubs
{
    public interface ISpectateHub
    {
        Task UpdatePoolParticipants(ICollection<PoolParticipant> poolers);

        Task UpdatePoolParticipantPicks(string poolerGuid, IEnumerable<Player> players, string pickedPlayerId);

        Task UpdatePickingPoolParticipant(string poolerGuid);

        Task UpdateNextPickingPoolParticipant(string poolerGuid);

        Task PicksCompleted();
    }
}