﻿using LePooler.Common.Constants;
using LePooler.Data.Contexts;
using LePooler.Data.Entities;
using LePooler.Logic.Pools.PlayerSelection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace LePooler.API.Hubs
{
    [Authorize(Policies.Authenticated)]
    public class SpectateHub : Hub<ISpectateHub>
    {
        private LePoolerContext _context;
        private IEntitiesLoader _entitiesLoader;
        private IPicksLogic _picksLogic;

        public SpectateHub(LePoolerContext context, IEntitiesLoader entitiesLoader, IPicksLogic picksLogic)
        {
            _context = context;
            _entitiesLoader = entitiesLoader;
            _picksLogic = picksLogic;
        }

        public override async Task OnConnectedAsync()
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, GetAuthenticatedPoolGuid());
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, GetAuthenticatedPoolGuid());
            await base.OnDisconnectedAsync(exception);
        }

        [Authorize(Policies.PoolAdmin)]
        public async Task UpdateSpectatorPoolParticipants()
        {
            var pool = _context.Pools.Find(new Guid(GetAuthenticatedPoolGuid()));
            var poolersInPickOrder = _picksLogic.GetPoolParticipantsInPickOrder(pool);

            await Clients.OthersInGroup(GetAuthenticatedPoolGuid()).UpdatePoolParticipants(poolersInPickOrder);

            var nextPoolersToPick = _picksLogic.GetNextPoolersToPick(2, new Guid(GetAuthenticatedPoolGuid()));
            nextPoolersToPick.TryGetValue(1, out var current);
            nextPoolersToPick.TryGetValue(2, out var next);
            await Clients.OthersInGroup(GetAuthenticatedPoolGuid()).UpdatePickingPoolParticipant(current.ToString());
            await Clients.OthersInGroup(GetAuthenticatedPoolGuid()).UpdateNextPickingPoolParticipant(next.ToString());
        }

        [Authorize(Policies.PoolAdmin)]
        public async Task UpdateSpectatorPicks(string poolerGuid, string pickedPlayerId)
        {
            var pool = _context.Pools.Find(new Guid(GetAuthenticatedPoolGuid()));
            _entitiesLoader.LoadPoolParticipants(pool, true, true, true, true, true);
            var poolParticipant = pool.PoolParticipants.Single(p => p.PoolerGuid.ToString() == poolerGuid);
            await Clients.OthersInGroup(GetAuthenticatedPoolGuid()).UpdatePoolParticipantPicks(poolerGuid, poolParticipant.Picks.Select(p => p.Player), pickedPlayerId);

            var nextPoolersToPick = _picksLogic.GetNextPoolersToPick(2, new Guid(GetAuthenticatedPoolGuid()));
            nextPoolersToPick.TryGetValue(1, out var current);
            nextPoolersToPick.TryGetValue(2, out var next);
            await Clients.OthersInGroup(GetAuthenticatedPoolGuid()).UpdatePickingPoolParticipant(current.ToString());
            await Clients.OthersInGroup(GetAuthenticatedPoolGuid()).UpdateNextPickingPoolParticipant(next.ToString());
        }

        [Authorize(Policies.PoolAdmin)]
        public async Task NotifyPicksCompleted()
        {
            await Clients.OthersInGroup(GetAuthenticatedPoolGuid()).PicksCompleted();
        }

        private string GetAuthenticatedPoolGuid()
        {
            return Common.Utilities.Authentication.GetPoolGuidClaim(Context.User.Identity);
        }
    }
}