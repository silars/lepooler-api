﻿using AngleSharp;
using LePooler.Common.Extensions;
using LePooler.Scrapers.Interfaces;
using LePooler.Scrapers.Outputs;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace LePooler.Scrapers.Marqueur
{
    public class MarqueurInjuriesScraper : IInjuriesScraper
    {
        private readonly Dictionary<string, int> MonthNameToNumber = new Dictionary<string, int>()
        {
            {"JANVIER", 1 },
            {"FÉVRIER", 2 },
            {"MARS", 3 },
            {"AVRIL", 4 },
            {"MAI", 5 },
            {"JUIN", 6 },
            {"JUILLET", 7 },
            {"AOÛT", 8 },
            {"SEPTEMBRE", 9 },
            {"OCTOBRE", 10 },
            {"NOVEMBRE", 11 },
            {"DÉCEMBRE", 12 }
        };

        private const string MARQUEUR_INJURIES_URL = "https://www.marqueur.com/hockey/stats/nhl/injuries.php";

        public async Task<IEnumerable<ScrapedInjury>> GetInjuries()
        {
            var config = Configuration.Default.WithDefaultLoader();
            var context = BrowsingContext.New(config);
            var doc = await context.OpenAsync(MARQUEUR_INJURIES_URL);

            var teamRows = doc.QuerySelectorAll(".table .w3-row");

            var scrapedInjuries = new List<ScrapedInjury>();
            foreach (var teamRow in teamRows)
            {
                var teamCity = teamRow.QuerySelector("div.w3-third > table a").TextContent;
                var teamName = teamRow.QuerySelector("div.w3-third > table").TextContent.Replace(teamCity, $"{teamCity} ");
                var injuredPlayers = teamRow.QuerySelectorAll("div.w3-twothird > div");

                foreach (var injuredPlayer in injuredPlayers)
                {
                    var injuredPlayerName = injuredPlayer.FirstElementChild.TextContent;
                    var injuryDescription = injuredPlayer.LastElementChild.TextContent;

                    var injury = new ScrapedInjury()
                    {
                        PlayerName = injuredPlayerName,
                        TeamName = teamName,
                        StartDate = DateTime.MinValue,
                        Description = injuryDescription
                    };
                    scrapedInjuries.Add(injury);
                }
            }

            return scrapedInjuries;
        }
    }
}