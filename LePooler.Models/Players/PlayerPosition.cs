﻿using LePooler.Common.JsonConverters;
using System.ComponentModel;
using System.Text.Json.Serialization;

namespace LePooler.Models.Players
{
    [JsonConverter(typeof(JsonDescriptionEnumConverter))]
    public enum PlayerPosition
    {
        None,

        [Description("Attaquant")]
        Offense,

        [Description("Défenseur")]
        Defense,

        [Description("Gardien")]
        Goaler
    }
}